import * as React from "react";
import { ENTER_KEY } from "./Constants";
import ReactDOM from "react-dom";

export interface ITodoEntryProps {
  addTodo: (value: string) => void;
}

export class TodoEntry extends React.Component<ITodoEntryProps> {
  render() {
    return (
      <input
        ref="newField"
        className="new-todo"
        placeholder="What needs to be done?"
        onKeyDown={this.handleNewTodoKeyDown}
        autoFocus={true}
      />
    );
  }

  handleNewTodoKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode !== ENTER_KEY) {
      return;
    }

    event.preventDefault();

    let element = ReactDOM.findDOMNode(this.refs.newField) as HTMLInputElement;
    if (element) {
      const val = element.value.trim();
      if (val) {
        this.props.addTodo(val);
        element.value = "";
      }
    }
  };
}
