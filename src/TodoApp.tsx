import React from "react";
import { Router } from "director/build/director";
import { TodoFooter } from "./TodoFooter";
import { ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import { ITodoModel } from "./interfaces";
import "./todomvc-app.css";
import "./todomvc-common.css";
import { TodoOverview } from "./TodoOverview";
import { TodoEntry } from "./TodoEntry";

export interface IAppProps {
  model: ITodoModel;
}

export interface IAppState {
  editing?: string;
  nowShowing?: string;
}

class TodoApp extends React.Component<IAppProps, IAppState> {
  public state: IAppState;

  constructor(props: IAppProps) {
    super(props);
    this.state = {
      nowShowing: ALL_TODOS
    };
  }

  public componentDidMount() {
    const setState = this.setState;
    const router = new Router({
      "/": setState.bind(this, { nowShowing: ALL_TODOS }),
      "/active": setState.bind(this, { nowShowing: ACTIVE_TODOS }),
      "/completed": setState.bind(this, { nowShowing: COMPLETED_TODOS })
    });
    router.init("/");
  }

  public render() {
    const todos = this.props.model.todos;

    const activeTodoCount = todos.reduce(function(accum, todo) {
      return todo.completed ? accum : accum + 1;
    }, 0);

    const completedCount = todos.length - activeTodoCount;

    return (
      <div>
        <header className="header">
          <h1>todos</h1>
          <TodoEntry addTodo={(v: string) => this.props.model.addTodo(v)} />
        </header>
        <TodoOverview
          todos={this.props.model.todos}
          count={activeTodoCount}
          completedCount={completedCount}
          nowShowing={this.state.nowShowing}
          toggleAll={v => this.props.model.toggleAll(v)}
          toggle={v => this.props.model.toggle(v)}
          destroy={v => this.props.model.destroy(v)}
          save={(t, v) => this.props.model.save(t, v)}
          edit={id => this.setState({ editing: id })}
          nowEditing={this.state.editing}
        />
        <TodoFooter
          count={activeTodoCount}
          completedCount={completedCount}
          nowShowing={this.state.nowShowing}
          onClearCompleted={() => this.props.model.clearCompleted()}
        />
      </div>
    );
  }
}

export { TodoApp };
