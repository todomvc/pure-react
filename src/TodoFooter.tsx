import { ACTIVE_TODOS, ALL_TODOS, COMPLETED_TODOS } from "./Constants";
import { Utils } from "./Utils";
import * as React from "react";

export interface ITodoFooterProps {
  completedCount: number;
  onClearCompleted: any;
  nowShowing?: string;
  count: number;
}

export class TodoFooter extends React.Component<ITodoFooterProps> {
  render() {
    const { count, completedCount } = this.props;
    if (!count && !completedCount) return null;

    const activeTodoWord = Utils.pluralize(count, "item");

    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{count}</strong> {activeTodoWord} left
        </span>
        <ul className="filters">
          {this.renderFilterLink(ALL_TODOS, "", "All")}
          {this.renderFilterLink(ACTIVE_TODOS, "active", "Active")}
          {this.renderFilterLink(COMPLETED_TODOS, "completed", "Completed")}
        </ul>
        {completedCount === 0 ? null : (
          <button className="clear-completed" onClick={this.clearCompleted}>
            Clear completed
          </button>
        )}
      </footer>
    );
  }

  renderFilterLink(filterName: string, url: string, caption: string) {
    const { nowShowing } = this.props;
    return (
      <li>
        <a
          href={"#/" + url}
          className={filterName === nowShowing ? "selected" : ""}
        >
          {caption}
        </a>{" "}
      </li>
    );
  }

  clearCompleted = () => {
    this.props.onClearCompleted();
  };
}
