import * as React from "react";
import { TodoItem } from "./TodoItem";
import { ACTIVE_TODOS, COMPLETED_TODOS } from "./Constants";
import { ITodo } from "./interfaces";

export interface ITodoOverviewProps {
  todos: ITodo[];
  count?: number;
  completedCount?: number;
  nowShowing?: string;
  toggleAll: (v: boolean) => void;
  toggle: (v: ITodo) => void;
  destroy: (v: ITodo) => void;
  save: (t: ITodo, v: string) => void;
  edit: (id?: string) => void;
  nowEditing?: string;
}

export class TodoOverview extends React.Component<ITodoOverviewProps> {
  render() {
    const { count, todos } = this.props;
    if (todos.length === 0) return null;
    return (
      <section className="main">
        <input
          className="toggle-all"
          id="toggle-all"
          type="checkbox"
          onChange={this.toggleAll}
          checked={count === 0}
        />
        <label htmlFor="toggle-all" />
        <ul className="todo-list">
          {this.getVisibleTodos().map(todo => (
            <TodoItem
              key={todo.id}
              todo={todo}
              onToggle={this.toggle.bind(this, todo)}
              onDestroy={this.destroy.bind(this, todo)}
              onEdit={this.edit.bind(this, todo)}
              editing={this.props.nowEditing === todo.id}
              onSave={this.save.bind(this, todo)}
              onCancel={e => this.cancel()}
            />
          ))}
        </ul>
      </section>
    );
  }

  getVisibleTodos() {
    return this.props.todos.filter(todo => {
      switch (this.props.nowShowing) {
        case ACTIVE_TODOS:
          return !todo.completed;
        case COMPLETED_TODOS:
          return todo.completed;
        default:
          return true;
      }
    });
  }

  public toggleAll(event: React.FormEvent) {
    const target: any = event.target;
    const checked = target.checked;
    this.props.toggleAll(checked);
  }

  public toggle(todoToToggle: ITodo) {
    this.props.toggle(todoToToggle);
  }

  public destroy(todo: ITodo) {
    this.props.destroy(todo);
  }

  public edit(todo: ITodo) {
    this.props.edit(todo.id);
  }

  public save(todoToSave: ITodo, text: string) {
    this.props.save(todoToSave, text);
    this.props.edit();
  }

  public cancel() {
    this.props.edit();
  }
}
